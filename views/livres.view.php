<?php
//temporisation par un buffer
ob_start();
if (!empty($_SESSION['alert'])):
?>
<div class="alert alert-<?=$_SESSION['alert']['type'] ?>" role="alert">
<?=$_SESSION['alert']['msg'] ?>
</div>
<?php endif ?>
<table class="table text-center">
    
    <tr class="table-dark">
        <th>Image</th>
        <th>Titre</th>
        <th>Nombre de pages</th>
        <th colspan="2">Action</th>
    </tr>
    <?php  //$livres est déclaré dans le controller livre   
        for ($i=0; $i < count($livres) ; $i++): ?>
        <tr>
        <td class="align-middle"><a href="<?= URL ?>/livres/l/<?= $livres[$i]->getId() ?>" ><img src="<?= URL ?>public/images/<?=$livres[$i]->getImage()?>" width="60p" /></a></td>
        <td class="align-middle"><a href="<?= URL ?>/livres/l/<?= $livres[$i]->getId() ?>"><?=$livres[$i]->getTitre() ?></a></td>
        <td class="align-middle"><?=$livres[$i]->getNbPages() ?></td>
        <td class="align-middle"><a href="<?= URL ?>/livres/m/<?= $livres[$i]->getId(); ?>" class="btn btn-warning">Modifier</a></td>
        <td class="align-middle">
            <form method="POST" action="<?= URL ?>/livres/s/<?= $livres[$i]->getId(); ?>" onsubmit="return confirm('Voulez-vous vraiment supprimer ce livre ?');">
            <button class="btn btn-danger" type="submit" >
                Supprimer
            </button>

            </form>
        </td>    
        </tr>     
    <?php endfor; ?>

</table>
<!-- d-block : donne l'espace disponible -->
<a href="<?= URL ?>livres/a" class="btn btn-success d-block">Ajouter</a>

<?php
$titre = "Les livres de la bibliothèque";
$content = ob_get_clean();
require "template.php";
