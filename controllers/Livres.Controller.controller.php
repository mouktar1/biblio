<?php
require_once "models/LivreManager.class.php";

class LivresController {   

    private $livreManager; 

    public function __construct()
    {
        $this->livreManager = new LivreManager;
        $this->livreManager->chargementLivres();
    }

    public function afficherLivres(){
        //ça 
        // $livreManager = $this->livreManager;
        // $livres = $livreManager->getLivres();
        //ou ça
        // $this->livreManager->getLivres();
        $livres = $this->livreManager->getLivres();//$livres sera récupéré dans livres.view.php
        require "views/livres.view.php";
        unset($_SESSION['alert']);
    }

    public function afficherLivre($id){
        $livre = $this->livreManager->getLivreById($id);
        require "views/affichageLivre.view.php";        
    }

    public function ajoutLivre(){
        require "views/ajoutLivre.view.php";
    }

    public function ajoutLivreValidation(){
        $file = $_FILES['image'];
        $repertoire = "public/images/";
        $nomImageAjoute = $this->ajoutImage($file,$repertoire);
        $this->livreManager->ajoutLivreBd($_POST['titre'],$_POST['nbPages'],$nomImageAjoute);
        $_SESSION['alert'] = [
            "type"=>"success",
            "msg"=>"Ajout réalisé"
        ];
        header('Location: '. URL . "livres");
    }

    private function ajoutImage($file, $dir){
        if(!isset($file['name']) || empty($file['name']))
            throw new Exception("Vous devez indiquer une image");
    
        if(!file_exists($dir)) mkdir($dir,0777); //0777 : droit accessible par n'importe qui
    
        $extension = strtolower(pathinfo($file['name'],PATHINFO_EXTENSION));//pour récuperer l'extension de l'image
        $random = rand(0,99999);//on génère un chiffre aléatoire (NB: on peut aussi generer la date et l'heur de l'image)
        $target_file = $dir.$random."_".$file['name'];//on rajoute le chiffre aléatoire après le nom de l'image
        //differents tests pour vérifier que l'image correspond à ce qui était attendu
        if(!getimagesize($file["tmp_name"]))
            throw new Exception("Le fichier n'est pas une image");
        if($extension !== "jpg" && $extension !== "jpeg" && $extension !== "png" && $extension !== "gif")
            throw new Exception("L'extension du fichier n'est pas reconnu");
        if(file_exists($target_file))
            throw new Exception("Le fichier existe déjà");
        if($file['size'] > 500000)
            throw new Exception("Le fichier est trop gros");
        if(!move_uploaded_file($file['tmp_name'], $target_file))  // move_uploaded_file : pour ajouter l'image directement à notre dossier
            throw new Exception("l'ajout de l'image n'a pas fonctionné");
        else return ($random."_".$file['name']);
    }

    public function suppressionLivre($id){
        $nomImage = $this->livreManager->getLivreById($id)->getImage();
        unlink("public/images/".$nomImage);
        $this->livreManager->suppressionLivreBD($id);
        $_SESSION['alert'] = [
            "type"=>"success",
            "msg"=>"Suppression réalisée"
        ];
        header('Location: '. URL . "livres");
    }

    public function modificationLivre($id){
        $livre = $this->livreManager->getLivreById($id);
        require "./views/modifierLivre.view.php";
    }

    public function modificationLivreValidation(){
        $imageActuelle = $this->livreManager->getLivreById($_POST['identifiant'])->getImage();
        $file = $_FILES['image'];

        if($file['size'] > 0){
            unlink("public/images/".$imageActuelle);
            $repertoire = "public/images/";
            $nomImageToAdd = $this->ajoutImage($file,$repertoire);
        } else {
            $nomImageToAdd = $imageActuelle;
        }
        $this->livreManager->modificationLivreBD($_POST['identifiant'],$_POST['titre'],$_POST['nbPages'],$nomImageToAdd);
        $_SESSION['alert'] = [
            "type"=>"success",
            "msg"=>"Modification réalisée"
        ];
        header('Location: '. URL . "livres");
    }
}