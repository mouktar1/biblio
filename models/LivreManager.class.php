<?php
require "models/Model.class.php";
require "models/Livre.class.php";

class LivreManager extends Model{

    private $livres;//tableau de Livre

    public function __construct()
    {
        
    }

    public function ajoutLivre($livre){
        $this->livres[]=$livre;
    }

    public function getLivres(){
        return $this->livres;
    }

    public function chargementLivres(){
        //on récupère la connexion et on prepare la requete
        $req = $this->getBdd()->prepare("SELECT * FROM livres ORDER BY id DESC");
        $req->execute();  //on execute la requete
        $mesLivres = $req->fetchAll(PDO::FETCH_ASSOC); //on récupère l'ensemble des valeurs en évitant les doublants
        $req->closeCursor(); //on termine la requete 

        foreach ($mesLivres as $livre) {
            $l = new Livre($livre["id"],$livre["titre"],$livre["nbPages"],$livre["image"]);
            $this->ajoutLivre($l);
        }
    }

    public function getLivreById($id){
        for($i=0; $i < count($this->livres);$i++){
            if($this->livres[$i]->getId() === $id){
                return $this->livres[$i];
            }
        }
        throw new Exception("Le livre n'existe pas.");
        
    }

    public function ajoutLivreBd($titre,$nbPages,$image){
        $req = "
        INSERT INTO livres (titre, nbPages, image)
        values (:titre, :nbPages, :image)";
        $stmt = $this->getBdd()->prepare($req);
        $stmt->bindValue(":titre",$titre,PDO::PARAM_STR);
        $stmt->bindValue(":nbPages",$nbPages,PDO::PARAM_INT);
        $stmt->bindValue(":image",$image,PDO::PARAM_STR);
        $resultat = $stmt->execute();
        $stmt->closeCursor();

        if($resultat > 0){
            $livre = new Livre($this->getBdd()->lastInsertId(),$titre,$nbPages,$image);
            $this->ajoutLivre($livre);
        }        
    }

    public function suppressionLivreBD($id){
        $req = "delete from livres where id =:idLivre";
        $stmt = $this->getBdd()->prepare($req);
        $stmt->bindValue(":idLivre",$id,PDO::PARAM_INT);
        $resultat = $stmt->execute();
        $stmt->closeCursor();
        if($resultat > 0){
            $livre = $this->getLivreById($id);
            unset($livre);
        }
    }

    public function modificationLivreBD($id,$titre,$nbPages,$image){
        $req = "
        update livres 
        set titre = :titre, nbPages = :nbPages, image = :image
        where id = :id";
        $stmt = $this->getBdd()->prepare($req);
        $stmt->bindValue(":id",$id,PDO::PARAM_INT);
        $stmt->bindValue(":titre",$titre,PDO::PARAM_STR);
        $stmt->bindValue(":nbPages",$nbPages,PDO::PARAM_INT);
        $stmt->bindValue(":image",$image,PDO::PARAM_STR);
        $resultat = $stmt->execute();
        $stmt->closeCursor();

        if($resultat > 0){
            $this->getLivreById($id)->setTitre($titre);
            $this->getLivreById($id)->setTitre($nbPages);
            $this->getLivreById($id)->setTitre($image);
        }
    }

}