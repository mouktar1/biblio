<?php

abstract class Model {//class jamais instanciable directement 

    private static $pdo;// contiendra l'instance de la connexion la BDD

    private static function setBdd(){
        //on crée la connexion 
        self::$pdo = new PDO("mysql:host=localhost;dbname=biblio;charset=utf8",'root','');
        //setAttribute : permet de créer la connexion et de placer dans $pdo
        //on définit deux élèments de la PDO qui permettent de gérer les erreurs
        self::$pdo->setAttribute(PDO::ATTR_ERRMODE,pdo::ERRMODE_WARNING);
    }

    protected function getBdd(){
        //si il y n'a pas une connexion parametré, on le crée
        if(self::$pdo == null){
            //on appel setBdd() à un seul moment
            self::setBdd();
        }
        //sinon on retourne celui existant
        return self::$pdo;
    }
    //NB: Cette classe sera hérité par toute classe qui aura besoin à l'accès à la BDD

}